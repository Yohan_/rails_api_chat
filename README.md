# Rails - API - chat

Proof of concept for a chat server based on Rails and using the Rails Action Cable feature (WebSockets).  
The Rails application is in API mode.

***
### Table of contents

- [1. Getting Started](#markdown-header-1-getting-started)
	* Prerequisites
	* Installation
- [2. Usage](#markdown-header-2-usage)
- [3. Action Cable](#markdown-header-3-action-cable)
- [4. API definition](#markdown-header-4-api-definition)
	* Default configuration
	* Authentication
	* [4.1 USERS](#markdown-header-41-users)
		+ Get all users
		+ Get a user
		+ Create a user
	* [4.2 ROOMS](#markdown-header-42-rooms)
		+ Get all rooms
		+ Get a room
		+ Create a room
	* [4.3 MESSAGES](#markdown-header-43-messages)
		+ Get all messages
		+ Get a message
		+ Create a message
- [5. Known Issues](#markdown-header-5-known-issues)

Table of contents generated with [markdown-toc](http://ecotrust-canada.github.io/markdown-toc/)

***

## 1. Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- GIT
- Rails
- MySQL

### Installation

1. Get the project locally

	`$ git clone https://Yohan_@bitbucket.org/Yohan_/rails_api_chat.git`

	`$ cd rails_api_chat`

	`$ bundle install`

2. MySQL database configuration.

	`$ vi config/database.yml`

	Check/update the

	* database
	* username 
	* password 

	in the default and development sections.

3. Database set up.

		$ rake db:setup

## 2. Usage

1. In your project directory, start the server application:  
	`$ rails server`
2. Use the [Rails - API - chat - client](https://bitbucket.org/Yohan_/rails_api_chat_client) application to chat.

3. Use Curl or some API client software (like [Postman](https://www.getpostman.com)) to send messages: see the [API definition](#markdown-header-4-api-definition) below.  
Your messages will be broadcasted to the users using the client application.

## 3. Action Cable

- url: ws://localhost:3003/cable
- port: 3003 ***/!\ not rails default port***
- single channel: chat

The url can be modified here:

	$ vi config/environments/development.rb
Update the line:

	# Action Cable uri
	config.action_cable.url = 'ws://localhost:3003/cable'

## 4. API definition

### Default configuration

The default configuration of the API is:

- url: http://localhost:3003/v1/
- port: 3003 ***/!\ not rails default port***

### Authentication

All call of the API, except the user creation, need an authentication.
The authentication must be done by the HTTP header.

- Field name: ```Authorization```
- Field value: ```Token token=<:TOKEN:>```

A default user exist in DB to be used by the API. The token of this user is: ```1234```

### 4.1 USERS
***
#### Get all users
***

* **Authentication needed:** Yes
* **Method:** `GET`
* **URL:** /users/
* **URL Params:** None
* **Data Params:** None
* **Success Response:**
	* **Code:** 200
	* **Content:**

			{"id":1,"name":"Api Guy","token":"1234","created_at":"2017-06-09T10:10:10.000Z","updated_at":"2017-06-09T10:10:10.000Z"},{"id":2,"name":"User","token":"oL8PCMzU4PgkrESEbS5ZgSdH","created_at":"2017-06-09T10:10:10.000Z","updated_at":"2017-06-09T10:10:10.000Z"}
 
* **Error Response:**
	* **Code:** 401 UNAUTHORIZED
	* **Content:** `{"error":"Access denied"}`

* **Sample Call:** (using Curl)

		curl -H "Authorization: Token token=1234" http://localhost:3003/v1/users/

***
#### Get a user
***

* **Authentication needed:** Yes
* **Method:** `GET`
* **URL:** /users/:id
* **URL Params:**
	* **Required:** `id=<:integer:>` 
* **Data Params:** None
* **Success Response:**
	* **Code:** 200
	* **Content:**

			{"id":1,"name":"Api Guy","token":"1234","created_at":"2017-06-09T10:10:10.000Z","updated_at":"2017-06-09T10:10:10.000Z"}
 
* **Error Response:**
	* **Code:** 401 UNAUTHORIZED
	* **Content:** `{"error":"Access denied"}`

* **Sample Call:** (using Curl)

		curl -H "Authorization: Token token=1234" http://localhost:3003/v1/users/1/

***
#### Create a user
***

* **Authentication needed:** No
* **Method:** `POST`
* **URL:** /users/
* **URL Params:** None
* **Data Params:**
	* **Required:** `user[name]=<:string:>` 
* **Success Response:**
	* **Code:** 200
	* **Content:**

			{"id":1,"name":"Api Guy","token":"1234","created_at":"2017-06-09T10:10:10.000Z","updated_at":"2017-06-09T10:10:10.000Z"}
 
* **Error Response:**
	* **Code:** 401 UNAUTHORIZED
	* **Content:** `{"error":"Access denied"}`

* **Sample Call:** (using Curl)

		curl -X POST -d "user[name]=Bob" http://localhost:3003/v1/users/

### 4.2 ROOMS
***
#### Get all rooms
***

* **Authentication needed:** Yes
* **Method:** `GET`
* **URL:** /rooms/
* **URL Params:** None
* **Data Params:** None
* **Success Response:**
	* **Code:** 200
	* **Content:**

			{"id":1,"name":"my room","created_at":"2017-06-09T10:10:10.000Z","updated_at":"2017-06-09T10:10:10.000Z"},{"id":1,"name":"my room 2","created_at":"2017-06-09T10:10:10.000Z","updated_at":"2017-06-09T10:10:10.000Z"}
 
* **Error Response:**
	* **Code:** 401 UNAUTHORIZED
	* **Content:** `{"error":"Access denied"}`

* **Sample Call:** (using Curl)

		curl -H "Authorization: Token token=1234" http://localhost:3003/v1/rooms/

***
#### Get a room
***

* **Authentication needed:** Yes
* **Method:** `GET`
* **URL:** /rooms/:id
* **URL Params:**
	* **Required:** `id=<:integer:>` 
* **Data Params:** None
* **Success Response:**
	* **Code:** 200
	* **Content:**

			{"id":1,"name":"my room","created_at":"2017-06-09T10:10:10.000Z","updated_at":"2017-06-09T10:10:10.000Z"}
 
* **Error Response:**
	* **Code:** 401 UNAUTHORIZED
	* **Content:** `{"error":"Access denied"}`

* **Sample Call:** (using Curl)

		curl -H "Authorization: Token token=1234" http://localhost:3003/v1/rooms/1/

***
#### Create a room
***

* **Authentication needed:** Yes
* **Method:** `POST`
* **URL:** /rooms/
* **URL Params:** None
* **Data Params:**
	* **Required:** `room[name]=<:string:>` 
* **Success Response:**
	* **Code:** 200
	* **Content:**

			{"id":1,"name":"my room","created_at":"2017-06-09T10:10:10.000Z","updated_at":"2017-06-09T10:10:10.000Z"}
 
* **Error Response:**
	* **Code:** 401 UNAUTHORIZED
	* **Content:** `{"error":"Access denied"}`

* **Sample Call:** (using Curl)

		curl -H "Authorization: Token token=1234" -X POST -d "room[name]=A room" http://localhost:3003/v1/rooms/

### 4.3 MESSAGES
***
#### Get all messages
***

With optional filter by room ID.

* **Authentication needed:** Yes
* **Method:** `GET`
* **URL:** /messages/{?room_id=:room_id}
* **URL Params:**
	* **Optionnal:** `room_id=<:integer:>`
* **Data Params:** None
* **Success Response:**
	* **Code:** 200
	* **Content:**

			{"id":1,"user_id":1,"room_id":1,"body":"hello!","created_at":"2017-06-09T10:10:10.000Z","updated_at":"2017-06-09T10:10:10.000Z"},{"id":2,"user_id":2,"room_id":1,"body":"hi!","created_at":"2017-06-09T10:10:10.000Z","updated_at":"2017-06-09T10:10:10.000Z"}
 
* **Error Response:**
	* **Code:** 401 UNAUTHORIZED
	* **Content:** `{"error":"Access denied"}`

* **Sample Call:** (using Curl)

	`curl -H "Authorization: Token token=1234" http://localhost:3003/v1/messages/`

	`curl -H "Authorization: Token token=1234" http://localhost:3003/v1/messages/\?room_id\=1`

***
#### Get a message
***

* **Authentication needed:** Yes
* **Method:** `GET`
* **URL:** /messages/:id
* **URL Params:**
	* **Required:** `id=<:integer:>` 
* **Data Params:** None
* **Success Response:**
	* **Code:** 200
	* **Content:**

			{"id":1,"user_id":1,"room_id":1,"body":"hello!","created_at":"2017-06-09T10:10:10.000Z","updated_at":"2017-06-09T10:10:10.000Z"}
 
* **Error Response:**
	* **Code:** 401 UNAUTHORIZED
	* **Content:** `{"error":"Access denied"}`

* **Sample Call:** (using Curl)

		curl -H "Authorization: Token token=1234" http://localhost:3003/v1/messages/1/

***
#### Create a message
***

* **Authentication needed:** Yes
* **Method:** `POST`
* **URL:** /rooms/
* **URL Params:** None
* **Data Params:**
	* **Required:** `message[user_id]=<:integer:>` 
	* **Required:** `message[room_id]=<: integer:>` 
	* **Required:** `message[body]=<:string:>` 
* **Success Response:**
	* **Code:** 200
	* **Content:**

			{"id":1,"user_id":1,"room_id":1,"body":"hello!","created_at":"2017-06-09T10:10:10.000Z","updated_at":"2017-06-09T10:10:10.000Z"}
 
* **Error Response:**
	* **Code:** 401 UNAUTHORIZED
	* **Content:** `{"error":"Access denied"}`

* **Sample Call:** (using Curl)

		curl -H "Authorization: Token token=1234" -X POST -d "message[user_id]=1" -d "message[room_id]=1" -d "message[body]=Hello" http://localhost:3003/v1/messages/

## 5. Known Issues

- There is a unique Action Cable chat channel, so the messages will belong to a unique room but will be broadcasted to all rooms when sent.
- The application don't handle correctly the 404 and 500 errors.
- Security: as a proof of concept, we assume that the security is not assured (no token lifetime management, no HTTPS, etc...).