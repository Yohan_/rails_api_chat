class AddUserIdAndRoomIdToMessages < ActiveRecord::Migration[5.1]
  def change
  	add_column :messages, :user_id, :bigint, null: false, :after => :id
    add_column :messages, :room_id, :bigint, null: false, :after => :user_id
    add_foreign_key :messages, :users
  	add_foreign_key :messages, :rooms
  end
end
