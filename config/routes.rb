Rails.application.routes.draw do
	namespace :v1 do
		resources :messages
		resources :rooms
		resources :users
	end
	# Actioncable server
	mount ActionCable.server, at: '/cable'
end
