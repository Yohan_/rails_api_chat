module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user
    def connect
      @current_user = auth_current_user
    end

    private
      def auth_current_user
        # use the token instead of the user id
        # this was, token invalidation etc will be effective
        if current_user = User.find_by(token: current_user_token)
          current_user
        else
          reject_unauthorized_connection
        end
      end

      def current_user_token
        if cookies[:user_info].present?
          JSON.parse(cookies[:user_info])['token'].to_s
        end
      end
  end
end