module V1
  class MessagesController < ApplicationController
    before_action :set_message, only: [:show, :update, :destroy]

    # GET /messages
    def index
      if params[:room_id]
        @messages = Message.find_by_room(params[:room_id])
      else
        @messages = Message.all
      end

      render json: @messages
    end

    # GET /messages/1
    def show
      render json: @message
    end

    # POST /messages
    def create
      @message = Message.new(message_params)
      @message.user_id = self.current_user.id
      if @message.save
        ActionCable.server.broadcast 'chat', message: @message.body, user: self.current_user.name
        #ActionCable.server.broadcast 'room_' + message_params[:room_id], message: @message.body, user: self.current_user.name
        render json: @message, status: :created
      else
        render json: @message.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /messages/1
    def update
      if @message.update(message_params)
        render json: @message
      else
        render json: @message.errors, status: :unprocessable_entity
      end
    end

    # DELETE /messages/1
    def destroy
      @message.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_message
        @message = Message.find(params[:id])
        # TODO: global ErrorHandler module needed
        rescue ActiveRecord::RecordNotFound => error
          render_error(error, :not_found)
      end

      # Only allow a trusted parameter "white list" through.
      def message_params
        params.require(:message).permit(:room_id, :body)
      end
  end
end