module V1
	class ApplicationController < ActionController::API
		include ActionController::HttpAuthentication::Token::ControllerMethods
		before_action :require_login
		attr_reader :current_user

		def require_login
			authenticate_token || render_error("Access denied", :unauthorized)
		end

		protected

		# response status
		# https://ruby-doc.org/stdlib-2.4.1/libdoc/net/http/rdoc/Net/HTTP.html#class-Net::HTTP-label-HTTP+Response+Classes
		def render_error(error, status)
			render json: {error: error.to_s}, status: status
		end

		private

		def authenticate_token
			authenticate_with_http_token do |token, options|
				@current_user = User.find_by(token: token)
			end
		end
	end
end
