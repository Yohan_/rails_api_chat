class Message < ApplicationRecord
	# TODO: resolve why the 'optional' parameter is mandatory at true
	#belongs_to :users
	#belongs_to :rooms
	belongs_to :users, optional: true
	belongs_to :rooms, optional: true

	validates :body, presence: true

	def self.find_by_room (room_id)
		where(room_id: room_id).order(created_at: :asc)
	end
end
