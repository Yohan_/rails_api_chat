class User < ApplicationRecord
	has_secure_token

	has_many :messages
	has_many :rooms, through: :messages

	validates :name, presence: true, uniqueness: true, case_sensitive: false
end
